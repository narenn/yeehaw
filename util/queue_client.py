"""Utility module for queue operations."""
import asyncio
import json
import logging
from collections import namedtuple

import photonpump


Event = namedtuple("Event", ["data", "metadata", "type", "id", "source"])


class QueueClient:
    """Utility wrapper class for photonpump.Client. Includes a callback function
    to make it easier to write bots.
    """

    def __init__(
        self,
        host: str,
        port: int,
        stream: str,
        username: str = None,
        password: str = None,
    ) -> None:
        loop = asyncio.get_event_loop()
        if username is not None and password is not None:
            self.client = photonpump.connect(
                host=host, port=port, loop=loop, username=username, password=password
            )
        else:
            self.client = photonpump.connect(host=host, port=port, loop=loop)
        self.stream = stream

    async def run(self) -> None:
        """Actually start the client + event loop. Note that this function runs
        forever, as self.subcription.events just blocks until the next event.

        Since this is async, you're gonna want to do something along the lines of:

        import asyncio


        client = QueueClient(host, port, stream, sub_name)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(client.run())
        """
        await self.client.connect()
        logging.info("Subscribing to %s", self.stream)
        self.subscription = await self.client.subscribe_to(self.stream)
        async for event in self.subscription.events:
            data = json.loads(event.original_event.data)
            metadata = event.original_event.metadata
            _type = event.original_event.type
            _id = event.original_event.id
            e = Event(data=data, metadata=metadata, type=_type, id=_id, source=event)
            await self.on_event(e)

    async def on_event(self, event: photonpump.NewEventData) -> None:
        """Callback function - called when there's a new event in the stream we
        are subscribed to.

        Subclass QueueClient and override this function to easily get access to
        the photonpump.Event object. To make things easier for you, here's what
        that type looks like:

        class Event:
            id: UUID
            type: str
            data: Dict[str, str]
            metadata: Dict[str, str]
            source: NewEventData
        """
        pass
