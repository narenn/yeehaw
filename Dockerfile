FROM python:3.6-alpine as build
RUN apk add build-base bash
RUN pip install virtualenv
WORKDIR /yeehaw
COPY requirements-dev.txt /yeehaw/requirements-dev.txt
SHELL ["/bin/bash", "-c"]
RUN virtualenv env -p $(which python3)
RUN source env/bin/activate && pip3 install -r requirements-dev.txt

FROM alpine
RUN apk add --no-cache python3 ffmpeg
COPY --from=build /yeehaw/env /yeehaw/env
COPY . /yeehaw
ENV VIRTUAL_ENV /yeehaw/env
ENV PATH /yeehaw/env/bin
ENV PYTHONPATH /yeehaw:/yeehaw/env/lib/python3.6/site-packages
WORKDIR /yeehaw
CMD make
