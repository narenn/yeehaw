"""Simple echo test bot"""
import argparse
import asyncio
import json
import logging
import os

from util.queue_client import QueueClient, Event


class Echo(QueueClient):
    """Simple echo bot - returns the tokenized text to the source channel."""

    def __init__(self, *args, output_stream="text_output", **kwargs):
        self.output_stream = output_stream
        super().__init__(*args, **kwargs)

    async def on_event(self, event: Event) -> None:
        return_string = " ".join(event.data["tokenized_content"][1:])
        return_data = {"text": return_string, "channel_id": event.data["channel_id"]}
        await self.client.publish_event(
            stream=self.output_stream, type="text", body=return_data
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A discord bot.")
    parser.add_argument("--log", help="set the log level.", type=str)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--config", help="path to config json.", type=argparse.FileType("r")
    )
    group.add_argument("--env", help="read secrets from environment.", type=bool)
    arguments = parser.parse_args()

    try:
        logging_level = arguments.log.upper()
    except AttributeError:
        logging_level = "INFO"

    numeric_level = getattr(logging, logging_level)
    logging.basicConfig(level=numeric_level)

    if arguments.env:
        client = Echo(
            host=os.environ["MQ_HOST"],
            port=int(os.environ["MQ_PORT"]),
            stream=os.environ["STREAM"],
            output_stream=os.environ["OUTPUT"],
            username=os.environ["MQ_USER"],
            password=os.environ["MQ_PASS"],
        )
    else:
        config = json.load(arguments.config)
        client = Echo(
            host=config["mq_host"],
            port=config["mq_port"],
            stream=config["stream"],
            output_stream=config["output"],
            username=config["mq_user"],
            password=config["mq_pass"],
        )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.run())
