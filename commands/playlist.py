"""Playlist bot"""
import argparse
import asyncio
import json
import logging
import os
import fnmatch
from typing import Any, Dict, List, Tuple

from util.queue_client import QueueClient, Event


class Playlist(QueueClient):
    """Queues up + plays things on an rtmp url passed into __init__"""

    def __init__(
        self,
        output_url: str,
        search_index: str,
        queue_name: str,
        *args,
        output_stream: str = "text_output",
        index_locations: List[str] = ["/data/TV Shows", "/data/Movies"],
        allowed_users=None,
        **kwargs
    ) -> None:
        self.output_stream = output_stream
        self.output_url = output_url
        self.search_index = search_index
        self.queue_name = queue_name
        self.index_locations = index_locations
        self.allowed_users = allowed_users

        queue = self.get_queue()
        queue["playing"] = False
        self.set_queue(queue)

        super().__init__(*args, **kwargs)

    def get_queue(self) -> Dict[str, Any]:
        """Load the queue from disk. This and the following function should probably be
        modified to be property getters/setters.
        """
        try:
            with open(self.queue_name, "r") as f:
                queue = json.load(f)
        except FileNotFoundError:
            self.clear()
            with open(self.queue_name, "r") as f:
                queue = json.load(f)

        if "offset" not in queue.keys():
            queue["offset"] = 0

        if "playing" not in queue.keys():
            queue["playing"] = False

        if "files" not in queue.keys():
            queue["files"] = []

        return queue

    def set_queue(self, queue: Dict[str, str]) -> None:
        """Write the queue to disk."""
        with open(self.queue_name, "w") as f:
            json.dump(queue, f)

    def decrement(self, amount: int = 1):
        """Helper method to simplify queue operations"""
        queue = self.get_queue()
        queue["offset"] -= amount
        if queue["offset"] < 0:
            queue["offset"] = 0
        self.set_queue(queue)

    def increment(self, amount: int = 1):
        """Helper method to simplify queue operations"""
        queue = self.get_queue()
        queue["offset"] += amount
        if queue["offset"] >= len(queue["files"]):
            queue["offset"] = len(queue["files"]) - 1
        self.set_queue(queue)

    def search(self, event: Event) -> List[str]:
        """Load the search index json from disk + iterate through it and return a list
        of files that substring matched.
        """
        try:
            query = " ".join(event.data["tokenized_content"][2:])
        except IndexError:
            return []

        if query[0] != "*":
            query = "*" + query
        if query[-1] != "*":
            query += "*"
        logging.info("Searching for %s", query)

        try:
            with open(self.search_index, "r") as f:
                index = json.load(f)
        except FileNotFoundError:
            return []

        return sorted(fnmatch.filter(index, query))

    def append_to_queue(self, event: Event, fuzzy: bool = True) -> str:
        """Get the filename either by searching or from the query, and then append it
        to the queue. Return back the filename appended.
        """
        if fuzzy is True:
            results = self.search(event)
            if not results:
                return "Couldn't find any files by this query."
            filename = results[0]
        else:
            filename = " ".join(event.data["tokenized_content"][2:])

        queue = self.get_queue()

        queue["files"].append(filename)

        self.set_queue(queue)

        return 'Appended "{}" to queue successfully!'.format(filename)

    def append_all_to_queue(self, event: Event) -> str:
        """Write everything returned by the query to the queue after sorting
        alphabetically.
        """
        results = self.search(event)
        if not results:
            return "Couldn't find any files by this query."

        queue = self.get_queue()

        for f in results:
            queue["files"].append(f)

        self.set_queue(queue)

        return 'Appended all for query "{}" to queue.'.format(
            " ".join(event.data["tokenized_content"][2:])
        )

    def rehash(self) -> str:
        """Write a new index file to disk."""
        combined_dirs: List[Tuple[Any, List[Any], List[Any]]] = []
        for location in self.index_locations:
            combined_dirs += [x for x in os.walk(location)]

        output: List[str] = []
        for root, dirs, files in combined_dirs:
            for name in files:
                output.append(os.path.abspath(os.path.join(root, name)))

        with open(self.search_index, "w") as f:
            json.dump(output, f)

        return "Rehash successful!"

    async def play(self, quality: str) -> None:
        """Start playing by loading the queue from disk and playing the item at the
        current offset.
        """
        queue = self.get_queue()
        queue["playing"] = True
        self.set_queue(queue)

        quality_dict = {
            "low": [
                "-c:v",
                "libx264",
                "-b:v",
                "1500k",
                "-vf",
                "scale=720:-2",
                "-c:a",
                "aac",
                "-b:a",
                "128k",
            ],
            "medium": [
                "-c:v",
                "libx264",
                "-b:v",
                "3000k",
                "-vf",
                "scale=1280:-2",
                "-c:a",
                "aac",
                "-b:a",
                "192k",
            ],
            "high": [
                "-c:v",
                "libx264",
                "-b:v",
                "6500k",
                "-vf",
                "scale=1920:-2",
                "-c:a",
                "aac",
                "-b:a",
                "256k",
            ],
            "source": ["-c:v", "copy", "-c:a", "aac", "-b:a", "320k"],
        }

        while queue["playing"] is True:
            filename = queue["files"][queue["offset"]]
            args = (
                ["/usr/bin/ffmpeg", "-re", "-i", filename, "-pix_fmt", "yuv420p"]
                + quality_dict[quality]
                + ["-f", "flv", "-preset", "faster", self.output_url]
            )
            self.running = await asyncio.create_subprocess_exec(*args)

            await self.running.wait()

            queue = self.get_queue()
            old_offset = queue["offset"]

            self.increment()

            queue = self.get_queue()
            if old_offset == queue["offset"]:
                # Reached end of playlist
                queue["playing"] = False
                self.set_queue(queue)

    def stop(self) -> str:
        """Stop playing by setting 'playing' to false and terminating the running
        process.
        """
        queue = self.get_queue()
        if queue["playing"] is False:
            return "Playback is currently stopped."
        queue["playing"] = False
        self.set_queue(queue)

        self.running.terminate()

        return "Stopped successfully."

    def clear(self) -> str:
        """Clear the queue out."""
        with open(self.queue_name, "w") as f:
            json.dump({"playing": False, "offset": 0, "files": []}, f)

        return "Cleared queue successfully."

    def next(self) -> str:
        """Open the next file on the queue by terminating the current running file, if
        running. Otherwise, just increment the offset.
        """
        queue = self.get_queue()
        if queue["playing"] is True:
            self.running.terminate()
            return "Started next file."
        self.increment()
        return "Next item queued."

    def prev(self) -> str:
        """Decrement the offset twice then terminate the running file."""
        queue = self.get_queue()
        if queue["playing"] is True:
            self.decrement(amount=2)
            self.running.terminate()
            return "Started previous file."
        self.decrement()
        return "Previous item queued."

    def list(self) -> str:
        """Get the current queue + where the current offset is."""
        queue = self.get_queue()
        return_string = "Playing: {}\n\n".format(queue["playing"])
        for f, i in zip(queue["files"], range(len(queue["files"]))):
            # If the current file is what is playing/what will be playing, mark that.
            is_playing = queue["offset"] == i
            return_string += (
                "{}\n".format(f) if is_playing is False else "**{}** <-\n".format(f)
            )
        return return_string

    async def on_event(self, event: Event) -> None:
        def _queue() -> str:
            return self.append_to_queue(event)

        def _exactqueue() -> str:
            return self.append_to_queue(event, fuzzy=False)

        def _queueall() -> str:
            return self.append_all_to_queue(event)

        def _search() -> str:
            return str(self.search(event))

        def _play() -> str:
            try:
                quality = event.data["tokenized_content"][2]
            except IndexError:
                quality = "medium"

            if quality not in ["low", "medium", "high", "source"]:
                return "Quality not one of 'low', 'medium', 'high' or 'source'"

            queue = self.get_queue()
            if not queue["files"]:
                return "No files in queue."
            if queue["playing"] is True:
                return "Already playing."
            asyncio.ensure_future(self.play(quality))
            return "Started playing."

        if (
            self.allowed_users is not None
            and event.data["author_id"] not in self.allowed_users
        ):
            await self.client.publish_event(
                stream=self.output_stream,
                type="text",
                body={
                    "text": "User not authorized.",
                    "channel_id": event.data["channel_id"],
                },
            )
            return
        func_dict = {
            "search": _search,
            "queue": _queue,
            "exactqueue": _exactqueue,
            "queueall": _queueall,
            "rehash": self.rehash,
            "play": _play,
            "stop": self.stop,
            "clear": self.clear,
            "next": self.next,
            "prev": self.prev,
            "list": self.list,
            "help": lambda: "Commands are: {}".format(", ".join(func_dict.keys())),
        }

        try:
            command = event.data["tokenized_content"][1]
        except IndexError:
            command = "help"

        try:
            func = func_dict[command]
        except KeyError:
            func = lambda: "Unsupported command."

        await self.client.publish_event(
            stream=self.output_stream,
            type="text",
            body={"text": func(), "channel_id": event.data["channel_id"]},
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A discord bot.")
    parser.add_argument("--log", help="set the log level.", type=str)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--config", help="path to config json.", type=argparse.FileType("r")
    )
    group.add_argument("--env", help="read secrets from environment.", type=bool)
    arguments = parser.parse_args()

    try:
        logging_level = arguments.log.upper()
    except AttributeError:
        logging_level = "INFO"

    numeric_level = getattr(logging, logging_level)
    logging.basicConfig(level=numeric_level)

    if arguments.env:
        client = Playlist(
            host=os.environ["MQ_HOST"],
            port=int(os.environ["MQ_PORT"]),
            stream=os.environ["STREAM"],
            output_stream=os.environ["OUTPUT"],
            output_url=os.environ["OUTPUT_URL"],
            username=os.environ["MQ_USER"],
            password=os.environ["MQ_PASS"],
            search_index=os.environ["SEARCH_INDEX"],
            queue_name=os.environ["QUEUE_NAME"],
            index_locations=json.loads(os.environ["INDEX_LOCATIONS"]),
            allowed_users=json.loads(os.environ["ALLOWED_USERS"]),
        )
    else:
        config = json.load(arguments.config)
        client = Playlist(
            host=config["mq_host"],
            port=config["mq_port"],
            stream=config["stream"],
            output_stream=config["output"],
            output_url=config["output_url"],
            username=config["mq_user"],
            password=config["mq_pass"],
            search_index=config["search_index"],
            queue_name=config["queue_name"],
            index_locations=config["index_locations"],
            allowed_users=config["allowed_users"],
        )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.run())
