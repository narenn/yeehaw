"""Module to transmit commands to the corresponding streams to consume."""
import argparse
import asyncio
import json
import logging
import os

from util.queue_client import QueueClient, Event


class CommandDispatcher(QueueClient):
    """QueueClient class modified to write data with commads to the right new stream.
    Note that the proper dictionary containing the command:stream mappings must be
    provided on init. Any stream included in the list for the * key will get every
    message.
    """

    def __init__(self, *args, **kwargs):
        self.command_dict = kwargs.pop("commands")
        super().__init__(*args, **kwargs)

    async def on_event(self, event: Event) -> None:
        command = event.data["tokenized_content"][0]
        try:
            output_stream = self.command_dict[command]
            await self.client.publish_event(
                stream=output_stream, type="text", body=event.data
            )
        except KeyError:
            pass
        for stream in self.command_dict["*"]:
            await self.client.publish_event(stream=stream, type="text", body=event.data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A discord bot.")
    parser.add_argument("--log", help="set the log level.", type=str)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--config", help="path to config json.", type=argparse.FileType("r")
    )
    group.add_argument("--env", help="read secrets from environment.", type=bool)
    command_group = parser.add_mutually_exclusive_group(required=True)
    command_group.add_argument(
        "--command_json",
        help="json file containing commands and their corresponding streams.",
        type=argparse.FileType("r"),
    )
    command_group.add_argument(
        "--command_env", help="load commands from environment variable", type=bool
    )
    arguments = parser.parse_args()

    try:
        logging_level = arguments.log.upper()
    except AttributeError:
        logging_level = "INFO"

    numeric_level = getattr(logging, logging_level)
    logging.basicConfig(level=numeric_level)

    if arguments.command_env:
        commands = json.loads(os.environ["COMMAND_JSON"])
    else:
        commands = json.load(arguments.command_json)

    if arguments.env:
        client = CommandDispatcher(
            host=os.environ["MQ_HOST"],
            port=int(os.environ["MQ_PORT"]),
            stream=os.environ["STREAM"],
            username=os.environ["MQ_USER"],
            password=os.environ["MQ_PASS"],
            commands=commands,
        )
    else:
        config = json.load(arguments.config)
        client = CommandDispatcher(
            host=config["mq_host"],
            port=config["mq_port"],
            stream=config["stream"],
            username=config["username"],
            password=config["password"],
            commands=commands,
        )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.run())
