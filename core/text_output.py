"""Output queue items to discord channels"""
import argparse
import asyncio
import json
import logging
import os

import discord

from util.queue_client import QueueClient, Event


class TextOutput(QueueClient):
    """Subclassed queue client to output data to discord."""

    def __init__(self, discord_token: str, *args, **kwargs) -> None:
        self.discord_token = discord_token
        self.discord_client = discord.Client()
        super().__init__(*args, **kwargs)

    def start(self) -> None:
        """Start event loop + connect discord client, then connect message queue."""
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._start())

    async def _start(self) -> None:
        """Helper async function to actually start clients."""
        await self.discord_client.login(self.discord_token)
        asyncio.ensure_future(self.discord_client.connect())
        await self.run()

    async def on_event(self, event: Event) -> None:
        """On any event, try to output that to the corresponding discord channel."""
        output_channel = discord.Object(id=event.data["channel_id"])
        output_text = event.data["text"]
        await self.discord_client.send_message(
            output_channel, content=output_text[:1990]
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A discord bot.")
    parser.add_argument("--log", help="set the log level.", type=str)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--config", help="path to config json.", type=argparse.FileType("r")
    )
    group.add_argument("--env", help="read secrets from environment.", type=bool)
    arguments = parser.parse_args()

    try:
        logging_level = arguments.log.upper()
    except AttributeError:
        logging_level = "INFO"

    numeric_level = getattr(logging, logging_level)
    logging.basicConfig(level=numeric_level)

    if arguments.env:
        client = TextOutput(
            discord_token=os.environ["DISCORD_TOKEN"],
            host=os.environ["MQ_HOST"],
            port=int(os.environ["MQ_PORT"]),
            stream=os.environ["STREAM"],
            username=os.environ["MQ_USER"],
            password=os.environ["MQ_PASS"],
        )
    else:
        config = json.load(arguments.config)
        client = TextOutput(
            discord_token=config["token"],
            host=config["mq_host"],
            port=config["mq_port"],
            stream=config["stream"],
            username=config["username"],
            password=config["password"],
        )

    client.start()
