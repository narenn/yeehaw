"""Client that relays discord messages into event store. Pass it a config.json
or pass in secrets/config with environment variables.
"""
import argparse
import asyncio
import json
import logging
import os
import re
from typing import List, Dict

import discord
import photonpump


def _fix_tokens(
    token: str,
    mentions: List[Dict[str, str]],
    channel_mentions: List[Dict[str, str]],
    role_mentions: List[Dict[str, str]],
    emojis: List[Dict[str, str]],
) -> str:
    for id_codes in [mentions, channel_mentions, role_mentions, emojis]:
        token = _fix_codes(token, id_codes)
    return token


def _fix_codes(token: str, id_code: List[Dict[str, str]]) -> str:
    # What we're doing here is filtering the id code list to try and see if the
    # current token is a mention/emoji. if it isn't, then the len(mention) will be 0.
    # if it is, it'll be 1.
    def choose_mention(mention: dict, t: str = token) -> bool:
        return mention["id_code"] == t

    mention = list(filter(choose_mention, id_code))
    if len(mention) == 1:
        return mention[0]["name"]
    return token


def _create_dict(message: discord.Message) -> dict:
    """Converts a discord message to a dictionary for the message queue."""
    nickname = message.author.nick if message.author.nick is not None else ""
    mentions = [
        {"id_code": m.mention, "name": m.display_name} for m in message.mentions
    ]
    channel_mentions = [
        {"id_code": m.mention, "name": m.name} for m in message.channel_mentions
    ]
    role_mentions = [
        {"id_code": m.mention, "name": m.name} for m in message.role_mentions
    ]
    emojis = re.findall(r"<:\S+:\d+>", message.clean_content)
    emoji_dicts = [
        {"id_code": e, "name": ":{}:".format(e.split(":")[1])} for e in emojis
    ]
    tokenized_content = [
        _fix_tokens(m, mentions, channel_mentions, role_mentions, emoji_dicts)
        for m in message.content.split(" ")
    ]
    attachments = [a["url"] for a in message.attachments]
    return {
        "timestamp": str(message.timestamp),
        "author": message.author.name,
        "author_nickname": nickname,
        "author_discriminator": message.author.discriminator,
        "author_id": message.author.id,
        "content": message.clean_content,
        "raw_content": message.content,
        "tokenized_content": tokenized_content,
        "attachments": attachments,
        "mentions": mentions,
        "channel_mentions": channel_mentions,
        "role_mentions": role_mentions,
        "emojis": emoji_dicts,
        "channel_name": message.channel.name,
        "channel_id": message.channel.id,
        "server_name": message.server.name,
        "server_id": message.server.id,
    }


class Client(discord.Client):
    """We need to subclass the discord client object so that we can add in
    the message queue connection - unfortunately since discord.py expects
    you to use client.run() and not loop.run_until_complete, this hackiness
    is necessary.
    """

    def __init__(self, token, *args, **kwargs):
        self.mq_host = kwargs.get("mq_host")
        self.mq_port = kwargs.get("mq_port")
        self.token = token
        self.stream = kwargs.get("stream")
        self.mq_client = photonpump.connect(host=self.mq_host, port=self.mq_port)
        super().__init__(*args, **kwargs)

    def run(self, token: str = None):
        if token is None:
            token = self.token
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.mq_client.connect())
        super().run(token)

    async def on_ready(self):
        """Log when connected to discord."""
        logging.info("Logged in as user %s with ID %s", self.user.name, self.user.id)

    async def on_message(self, message):
        """Create a dictionary and dump that into the message queue"""
        if message.author.id == self.user.id:
            return

        logging.info(
            "Sending message from %s in #%s to the queue",
            message.author.name,
            message.channel.name,
        )
        await self.mq_client.publish_event(
            stream=self.stream, type="text", body=_create_dict(message)
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A discord bot.")
    parser.add_argument("--log", help="set the log level.", type=str)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--config", help="path to config json.", type=argparse.FileType("r")
    )
    group.add_argument("--env", help="read secrets from environment.", type=bool)
    arguments = parser.parse_args()

    try:
        logging_level = arguments.log.upper()
    except AttributeError:
        logging_level = "INFO"

    numeric_level = getattr(logging, logging_level)
    logging.basicConfig(level=numeric_level)

    if arguments.env:
        discord_client = Client(
            os.environ["DISCORD_TOKEN"],
            mq_host=os.environ["MQ_HOST"],
            mq_port=int(os.environ["MQ_PORT"]),
            stream=os.environ["STREAM_NAME"],
        )
    else:
        config = json.load(arguments.config)
        discord_client = Client(
            config["token"],
            mq_host=config["mq_host"],
            mq_port=config["mq_port"],
            stream=config["stream"],
        )

    discord_client.run()
