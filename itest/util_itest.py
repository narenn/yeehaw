"""Miscellaneous utility functions for integration testing."""
import asyncio
import os
from time import sleep
from typing import Dict, List, Mapping, Sequence, Union

import _pytest
import docker
import photonpump
import requests


class IncorrectEventCountException(BaseException):
    """Raised when there is an incorrect number of events."""

    pass


class MessageQueueNotFoundException(BaseException):
    """Raised when no message queue is found."""

    pass


def _wait_for_command_dispatcher(
    container: docker.models.containers.Container,
    sentinel: str = "is connected to eventstore",
):
    for line in container.logs(stream=True):
        if sentinel in str(line):
            return


async def send_data(
    client: photonpump.Client,
    data: Dict[str, Union[str, Sequence[str]]],
    input_stream: str,
    output_stream: str,
    container: docker.models.containers.Container,
    timeout: int = 10,
    expected_count: int = 1,
    event_loop: asyncio.AbstractEventLoop = None,
) -> List[photonpump.Event]:
    """Convenience function. Sends data to input queue, waits on result from output
    queue. Returns the event as soon as it shows up.
    """
    if event_loop is None:
        event_loop = asyncio.get_event_loop()
    events: List[photonpump.NewEventData] = []
    async with client as conn:
        await conn.publish_event(input_stream, "testing_action", body=data)
        while timeout > 0:
            try:
                events = await conn.get(output_stream)
            except (
                photonpump.exceptions.StreamNotFound,
                photonpump.exceptions.EventNotFound,
            ):
                pass
            await asyncio.sleep(1, loop=event_loop)
            timeout -= 1
    if len(events) != expected_count:
        logs = container.logs().decode("utf-8")
        raise IncorrectEventCountException(
            f"Incorrect Event count. Container output: {logs}"
        )
    return events


def wait_for_queue():
    """Wait for the queue container to be up."""
    retries = 20
    while retries > 0:
        try:
            r = requests.get("http://queue:2113")
            if r.status_code == 200:
                return
        except requests.exceptions.ConnectionError:
            pass
        sleep(1)
        retries -= 1
    raise MessageQueueNotFoundException("Timed out waiting for queue to be up.")


def create_and_wait_for_container(
    request: _pytest.fixtures.FixtureRequest,
    mq_name: str,
    docker_client: docker.client.DockerClient,
    network: docker.models.networks.Network,
    command: str,
    environment_extras: Mapping[str, str] = None,
):
    """Helper function to create and return the container we are itesting."""
    image_name = request.config.getoption("--image")
    if image_name is None:
        image_name = os.environ["IMAGE_NAME"]

    wait_for_queue()

    environment = {
        "MQ_HOST": mq_name,
        "MQ_PORT": 1113,
        "STREAM": "input",
        "SUB_NAME": "test_sub",
        "MQ_USER": "admin",
        "MQ_PASS": "changeit",
    }
    if environment_extras is not None:
        environment = {**environment, **environment_extras}

    container = docker_client.containers.run(
        image_name,
        network=network.id,
        detach=True,
        name="yeehaw",
        command=command,
        environment=environment,
    )

    _wait_for_command_dispatcher(container)

    return container
