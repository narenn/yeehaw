"""General use fixtures for integration tests."""
import docker
import photonpump
import pytest

from util_itest import wait_for_queue


class _Client(photonpump.Client):
    """Override photonpump.Client's init to make the heartbeat loop do nothing to
    suppress an annoying error message.
    """

    async def send_heartbeats(self):
        """Literally do nothing to kill an error message."""
        pass


def _connect(
    host="localhost",
    port=1113,
    discovery_host=None,
    discovery_port=2113,
    username=None,
    password=None,
    loop=None,
) -> photonpump.Client:
    """Modified copy/pasted photonpump.connect() function - the original doesn't
    actually pass the right loop to the heartbeat coroutine. This fixes that.
    """
    discovery = photonpump.get_discoverer(host, port, discovery_host, discovery_port)
    dispatcher = photonpump.MessageDispatcher(loop)
    connector = photonpump.Connector(discovery, dispatcher, loop=loop)

    credential = (
        photonpump.msg.Credential(username, password) if username and password else None
    )

    return _Client(connector, dispatcher, credential=credential)


def pytest_addoption(parser):
    """Make sure that we get an image to run (otherwise itests should fail)."""
    parser.addoption("--image", help="the yeehaw image name to run")


@pytest.fixture(scope="session")
def docker_client():
    """Just return the docker client to make writing other fixtures cleaner."""
    return docker.from_env()


@pytest.fixture
def network(
    docker_client: docker.client.DockerClient
) -> docker.models.networks.Network:
    """Fixture to create a network for the testing to be done in."""
    testing_container = docker_client.containers.get("yeehaw_itest")
    new_network = docker_client.networks.create("testing_network")
    new_network.connect(testing_container)
    yield new_network
    new_network.disconnect(testing_container)
    new_network.remove()


@pytest.fixture
def mq_image(docker_client: docker.client.DockerClient) -> docker.models.images.Image:
    """Make sure we have the eventstore image."""
    try:
        image = docker_client.images.get("eventstore/eventstore")
    except docker.errors.ImageNotFound:
        image = docker_client.images.pull("eventstore/eventstore", tag="latest")
    return image


@pytest.fixture
def message_queue(
    docker_client: docker.client.DockerClient,
    mq_image: docker.models.images.Image,
    network: docker.models.networks.Network,
) -> docker.models.containers.Container:
    """Actually start the message queue."""
    container = docker_client.containers.run(
        mq_image, network=network.id, detach=True, name="queue"
    )

    yield container

    container.remove(force=True, v=True)


@pytest.fixture
def message_queue_client(event_loop, message_queue):
    """Fixture to return a photonpump context manager. Due to some asyncio weirdness
    that I'm sick of trying to debug, the test function should use this as a context
    manager:

    @pytest.mark.asyncio
    async def test_foo(message_queue_client):
        async with message_queue_client as conn:
            await conn.do_things()
    """
    wait_for_queue()

    return _connect(
        host=message_queue.name,
        port=1113,
        username="admin",
        password="changeit",
        loop=event_loop,
    )
