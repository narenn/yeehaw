"""Test that the echo bot works as expected."""
import asyncio
import json

import _pytest
import docker
import photonpump
import pytest

from util_itest import create_and_wait_for_container, send_data


@pytest.fixture
def echo_bot_container(
    request: _pytest.fixtures.FixtureRequest,
    message_queue: docker.models.containers.Container,
    docker_client: docker.client.DockerClient,
    network: docker.models.networks.Network,
):
    """Start and return a running echo bot container."""
    command = "python commands/echo.py --env=True"
    env = {"OUTPUT": "output"}
    container = create_and_wait_for_container(
        request, message_queue.name, docker_client, network, command, env
    )

    yield container

    container.remove(force=True, v=True)


@pytest.mark.asyncio
async def test_echo(
    message_queue_client: photonpump.Client,
    event_loop: asyncio.AbstractEventLoop,
    echo_bot_container: docker.models.containers.Container,
):
    """Just a simple smoke test to make sure that the echo command is working."""

    data = {"channel_id": "1234567890", "tokenized_content": ["!echo", "test"]}
    events = await send_data(
        message_queue_client,
        data,
        "input",
        "output",
        echo_bot_container,
        event_loop=event_loop,
    )
    actual = json.loads(events[0].original_event.data)

    expected = {"channel_id": "1234567890", "text": "test"}

    assert expected == actual
