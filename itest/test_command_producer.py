"""Test that the command dispatcher works correctly."""
import asyncio
import json

import _pytest
import docker
import photonpump
import pytest

from util_itest import create_and_wait_for_container, send_data


@pytest.fixture(params=['{"*": ["output"]}', '{"!test": "output"}'])
def command_dispatcher_container(
    request: _pytest.fixtures.FixtureRequest,
    docker_client: docker.client.DockerClient,
    network: docker.models.networks.Network,
    message_queue: docker.models.containers.Container,
):
    """Create a yeehaw command dispatcher container."""
    command = "python core/command_dispatcher.py --env=True --command_env=True"
    env = {"COMMAND_JSON": request.param}
    container = create_and_wait_for_container(
        request, message_queue.name, docker_client, network, command, env
    )

    yield container

    container.remove(force=True, v=True)


@pytest.mark.asyncio
async def test_command_dispatcher(
    message_queue_client: photonpump.Client,
    event_loop: asyncio.AbstractEventLoop,
    command_dispatcher_container: docker.models.containers.Container,
):
    """Just a simple smoke test to make sure that the command dispatcher is working."""

    data = {
        "testing": "data",
        "please": "ignore",
        "tokenized_content": ["!test", "post"],
    }
    events = await send_data(
        message_queue_client,
        data,
        "input",
        "output",
        command_dispatcher_container,
        event_loop=event_loop,
    )
    output_data = json.loads(events[0].original_event.data)

    assert data == output_data
