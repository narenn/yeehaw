"""Test the text_producer module."""
import datetime
from unittest import mock

from core.text_producer import _create_dict, _fix_codes


def _create_msg(timestamp: datetime.datetime) -> mock.MagicMock:
    msg = mock.MagicMock()
    msg.timestamp = timestamp
    msg.author.nick = "foo"
    msg.author.name = "bar"
    msg.author.discriminator = "1234"
    msg.author.id = "42069"
    msg.clean_content = "testing testing 123"
    msg.content = "<!1000> testing testing 123"
    msg.mentions = [mock.MagicMock(), mock.MagicMock()]
    msg.mentions[0].mention = "foo"
    msg.mentions[0].display_name = "bar"
    msg.mentions[1].mention = "baz"
    msg.mentions[1].display_name = "test"
    msg.channel.name = "channel foo"
    msg.channel.id = "69420"
    msg.server.name = "server bar"
    msg.server.id = "1337"
    return msg


def test_text_producer():
    """Happy path test to make sure the right data gets passed to the mq"""
    now = datetime.datetime.now()
    msg = _create_msg(now)

    actual = _create_dict(msg)

    expected = {
        "timestamp": str(now),
        "author": "bar",
        "author_nickname": "foo",
        "author_discriminator": "1234",
        "author_id": "42069",
        "content": "testing testing 123",
        "raw_content": "<!1000> testing testing 123",
        "tokenized_content": ["<!1000>", "testing", "testing", "123"],
        "attachments": [],
        "mentions": [
            {"id_code": "foo", "name": "bar"},
            {"id_code": "baz", "name": "test"},
        ],
        "channel_mentions": [],
        "role_mentions": [],
        "emojis": [],
        "channel_name": "channel foo",
        "channel_id": "69420",
        "server_name": "server bar",
        "server_id": "1337",
    }
    assert actual == expected


def test_no_nickname():
    """Test that the correct nickname gets set when msg.author.nick is None"""
    now = datetime.datetime.now()
    msg = _create_msg(now)
    msg.author.nick = None

    actual = _create_dict(msg)
    assert actual["author_nickname"] == ""


def test_fix_mentions():
    """Happy path test for fixing mentions"""
    tok = "test"
    mention = [{"id_code": "test", "name": "newtoken"}]
    expected = "newtoken"
    actual = _fix_codes(tok, mention)
    assert expected == actual


def test_fix_mentions_noop():
    """Tests that mentions that don't correspond to any tokens are ignored."""
    tok = "test"
    mention = [{"id_code": "test1", "name": "newtoken"}]
    expected = "test"
    actual = _fix_codes(tok, mention)
    assert expected == actual
