SHELL := /bin/bash
.PHONY: all audit test utest itest clean

all: audit test

install: requirements-dev.log

requirements.log:
	virtualenv env -p $(shell which python3)
	source env/bin/activate && pip3 install -r requirements.txt | tee requirements.log

requirements-dev.log:
	virtualenv env -p $(shell which python3)
	source env/bin/activate && pip3 install -r requirements-dev.txt | tee requirements-dev.log

format: requirements-dev.log
	source env/bin/activate && black --exclude env .

audit: requirements-dev.log
	source env/bin/activate && mypy core itest util utest commands
	source env/bin/activate && black --check --quiet --exclude env core itest util utest commands
	source env/bin/activate && pylint --rcfile=pylintrc core itest util utest commands

test: itest utest

utest: requirements-dev.log
	source env/bin/activate && PYTHONPATH=$(shell pwd) pytest -svvv utest/

itest: requirements-dev.log
	docker build -t yeehaw .
	docker run -it --name yeehaw_itest -v /var/run/docker.sock:/var/run/docker.sock --rm yeehaw pytest -svvv itest --image yeehaw

clean:
	rm -rf env .mypy_cache
	docker rm -f $(docker ps -qa) || true
	rm requirements.log requirements-dev.log || true
